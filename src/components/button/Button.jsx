import {Component} from "react";

export class Button extends Component {
    render() {
        const {btnText, action, btnBackgroundColor} = this.props;
        return (
            <div>

                <button
                    onClick={action}
                    style={{background: btnBackgroundColor}}
                >
                    {btnText}
                </button>
            </div>
        );
    }
}