import {Component} from "react";
import {Button} from "../button/Button";
import styles from "./Modal.module.scss";

export class Modal extends Component {
    // state = {
    //     close: true
    // };

    render() {

        const {header, text, actions, closeButton} = this.props;

        return this.props.showModal ?  (
            <div className={styles.ModalWrapper}
                 onClick={(e) => e.target === e.currentTarget && this.props.closeModal()}>

                <div className={styles.Modal}>
                    <div className={styles.Header}>
                        <h3 className={styles.HeaderText}>{header}</h3>
                        {closeButton && (
                            <Button
                                btnBackgroundColor={"#ef5959"}
                                btnText={"X"}
                               action={this.props.closeModal}
                            />
                        )}

                    </div>
                    <main>
                        <p>{text}</p>
                        <div className={styles.ModalBtn}>{actions}</div>
                    </main>
                </div>
            </div>
        ) : null
    }
}