import "./App.css";
import {Component} from "react";
import {Button} from "./components/button/Button";
import {Modal} from "./components/modal/Modal";


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal1: false,
            showModal2: false,

        };
    }

    openModal1 = (event) => {
        this.setState({
            showModal1: true
        });
    };
    openModal2 = (event) => {
        this.setState({
            showModal2: true
        });
    };
    closeModal = () => {
        console.log("closeModal");
        this.setState({showModal1: false, showModal2: false});
    };


    render() {

        return (
            <div className="App">
                <Button
                    btnText={"Open first modal"}
                    btnBackgroundColor={"blue"}
                    action={this.openModal1}

                />
                <Button
                    btnText={"Open second modal"}
                    btnBackgroundColor={"yellow"}
                    action={this.openModal2}
                />

                {/*{this.state.showModal1 && <Modal/>}*/}
                <Modal
                    showModal={this.state.showModal1}
                    header={"Modal one"}
                    closeButton={true}
                    text={"text 1"}
                    closeModal={this.closeModal}
                    actions={[
                        <Button
                            btnText={"Close"}
                            btnBackgroundColor={"blue"}
                            action={this.closeModal}
                            key={Date.now()}
                        />,
                        <Button
                            btnText={"Ok"}
                            btnBackgroundColor={"blue"}
                            // onClick={this.openModal1}
                            key={Date.now() + 1}
                        />
                    ]}/>

                <Modal
                    showModal={this.state.showModal2}
                    header={"Modal 2"}
                    closeButton={false}
                    text={"text2"}
                    closeModal={this.closeModal}
                    actions={[
                        <Button
                            btnText={"Close"}
                            btnBackgroundColor={"green"}
                            action={this.closeModal}
                            key={Date.now()}
                        />,
                        <Button
                            btnText={"Ok"}
                            btnBackgroundColor={"green"}
                            // action={this.closeModal}
                            key={Date.now() + 1}
                        />
                    ]}/>


            </div>
        );
    }


}

export default App;
